//
//  SceneDelegate.swift
//  FoodSearch
//
//  Created by Kyle Ryan on 10/9/20.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        let window = UIWindow(windowScene: windowScene)
        window.rootViewController = UINavigationController(rootViewController: DependencyContainer.shared.makeFoodsViewController())
        window.makeKeyAndVisible()
        self.window = window
    }
}

