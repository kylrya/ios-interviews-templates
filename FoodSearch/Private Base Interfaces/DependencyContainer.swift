//
//  DependencyContainer.swift
//  FoodSearch
//
//  Created by Kyle Ryan on 10/9/20.
//

import Foundation

final class DependencyContainer {
    static let shared = DependencyContainer()
}

extension DependencyContainer: FoodsViewControllerFactory {
    func makeFoodsViewController() -> FoodsViewController {
        let foodStorage = FoodStorage()
        return FoodsViewController(foodStorage: foodStorage)
    }
}
