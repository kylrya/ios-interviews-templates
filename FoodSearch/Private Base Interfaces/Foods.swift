//
//  FoodData.swift
//  FoodSearch
//
//  Created by Kyle Ryan on 10/9/20.
//

import Foundation

struct Food: IFood, Equatable, Hashable, Codable {
    let id: String
    let title: String
    let imageURL: URL
    let isRecent: Bool
    let isTastyForYou: Bool
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case imageURL = "image_url"
        case isRecent = "is_recent"
        case isTastyForYou = "is_tfy"
    }
}

enum Foods {
    static var selectedFoods: [IFood] = []
    
    static let allFoods: [IFood] = {
        guard let filePath = Bundle.main.path(forResource: "Foods", ofType: "json") else {
            fatalError("Could not find Foods.json in Main Bundle")
        }
        
        let fileURL = URL(fileURLWithPath: filePath)
        
        do {
            let foods = try JSONDecoder().decode([Food].self, from: try Data(contentsOf: fileURL))
            return foods
        } catch {
            fatalError(error.localizedDescription)
        }
    }()
}
