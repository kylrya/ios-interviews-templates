//
//  FoodStorage.swift
//  FoodSearch
//
//  Created by Kyle Ryan on 10/9/20.
//

import Foundation

final class FoodStorage: IFoodStorage {
    func allFoods() -> [IFood] {
        do {
            sleep(5)
        }
        return Foods.allFoods
    }
}
