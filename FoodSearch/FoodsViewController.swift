//
//  ViewController.swift
//  FoodSearch
//
//  Created by Kyle Ryan on 10/9/20.
//

import UIKit

protocol FoodsViewControllerFactory {
    func makeFoodsViewController() -> FoodsViewController
}

final class FoodsViewController: UITableViewController {
    
    private let foodStorage: IFoodStorage
    
    init(foodStorage: IFoodStorage) {
        self.foodStorage = foodStorage
        super.init(nibName: String(describing: FoodsViewController.self),
                   bundle: Bundle(for: FoodsViewController.self))
        view.backgroundColor = .systemGray6
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
