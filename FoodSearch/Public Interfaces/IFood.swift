//
//  IFood.swift
//  FoodSearch
//
//  Created by Kyle Ryan on 10/12/20.
//

import Foundation

protocol IFood {
    var id: String { get }
    var title: String { get }
    var imageURL: URL { get }
    var isRecent: Bool { get }
    var isTastyForYou: Bool { get }
}
