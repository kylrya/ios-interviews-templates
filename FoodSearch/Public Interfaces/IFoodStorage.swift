//
//  IFoodStorage.swift
//  FoodSearch
//
//  Created by Kyle Ryan on 10/12/20.
//

import Foundation

protocol IFoodStorage {
    // Call to retrieve foods is thread blocking (ex: 5 seconds).
    func allFoods() -> [IFood]
}
