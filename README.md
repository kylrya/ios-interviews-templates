# README #

### What is this project? ###
* We provide a base Xcode project of a "Food Search" problem that will be described to you when the interview starts.
* Your interview will take place using screen sharing an Xcode window over a Zoom Video Call. 

### How do I get set up? ###

* Download and install the latest version of Xcode from the Mac App Store https://apps.apple.com/us/app/xcode/id497799835?mt=12.
* Ensure you can run this example project. If you have any difficulties running the template before the interview, please contact: kyle@twinhealth.com

### Who do I talk to? ###

* Please email kyle@twinhealth.com if you have any issues.
